package WebDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class selectdropdownassignment {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Akshay Kumar Reddy\\workspace\\raja1\\ProjectNew\\Libs\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
driver.get("https://demoqa.com/select-menu");
Thread.sleep(1000);
driver.findElement(By.id("withOptGroup")).click();
driver.findElement(By.xpath("//div[contains(text(),'Group 1, option 1')]")).click();
driver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]")).click();
driver.findElement(By.xpath("//div[contains(text(),'Mr.')]")).click();
Select select = new Select(driver.findElement(By.id("oldSelectMenu")));
java.util.List<WebElement> lst = select.getOptions();
System.out.println("The dropdown options are:");
for(WebElement options: lst)
    System.out.println(options.getText());
System.out.println("Select the Option by Index 7");
select.selectByIndex(7);
System.out.println("Select value is: " + select.getFirstSelectedOption().getText());
System.out.println("Select the Option by Text Magenta");
select.selectByVisibleText("Magenta");
System.out.println("Select value is: " + select.getFirstSelectedOption().getText());
System.out.println("Select the Option by value 6");
select.selectByValue("6");
System.out.println("Select value is: " + select.getFirstSelectedOption().getText());
//driver.findElement(By.xpath("//div[@class='  css-1hwfws3']")).click();
//Thread.sleep(1000);
//driver.findElement(By.xpath("//div[contains(text(),'Green')]")).click();

Select select1 = new Select(driver.findElement(By.id("cars")));
System.out.println("The dropdown options are -");
java.util.List<WebElement> options = select1.getOptions();
for(WebElement option: options)
    System.out.println(option.getText());
//Using isMultiple() method to verify if the element is multi-select, if yes go onto next steps else eit
if(select.isMultiple()){

    //Selecting option as 'Opel'-- ByIndex
    System.out.println("Select option Opel by Index");
    select1.selectByIndex(2);

    //Selecting the option as 'Saab'-- ByValue
    System.out.println("Select option saab by Value");
    select1.selectByValue("saab");

    // Selecting the option by text
    System.out.println("Select option Audi by Text");
    select1.selectByVisibleText("Audi");

    //Get the list of selected options
    System.out.println("The selected values in the dropdown options are -");

    java.util.List<WebElement> selectedOptions = select1.getAllSelectedOptions();

    for(WebElement selectedOption: selectedOptions)
        System.out.println(selectedOption.getText());
 // Deselect the value "Audi" by Index
    System.out.println("DeSelect option Audi by Index");
    select1.deselectByIndex(3);

    //Deselect the value "Opel" by visible text
    System.out.println("Select option Opel by Text");
    select1.deselectByVisibleText("Opel");

    //Validate that both the values are deselected
    System.out.println("The selected values after deselect in the dropdown options are -");
    java.util.List<WebElement> selectedOptionsAfterDeselect = select1.getAllSelectedOptions();

    for(WebElement selectedOptionAfterDeselect: selectedOptionsAfterDeselect)
        System.out.println(selectedOptionAfterDeselect.getText());

    //Step#8- Deselect all values
    select1.deselectAll();
}

driver.quit();


	}

}
